module.exports = function (grunt) {

    // nastaveni projektu
    grunt.initConfig({
        pkg: grunt.file.readJSON('package.json'),
        less: {
            options: {
                banner: '/*! <%= pkg.name %> <%= grunt.template.today("yyyy-mm-dd") %> */\n',
                sourceMap: true, // source map
                sourceMapRootpath: '/'
            },
            devel: {
                files: {
                    'build/css/layout.css': 'src/less/layout.less'
                }
            },
            build: {
                options: {
                    compress: true
                },
                files: [{
                    expand: true,
                    cwd: 'src/less/',  //zdrojovy adresar
                    src: ['**/*.less'], //nastavim aby se kompilovali soubory s koncovkou *.less vcetne podadresaru
                    dest: 'build/css/',  //vystupni adresar
                    ext: '.css'
                }]
            }
        }
    });

    // nacteni pluginu do gruntu
    grunt.loadNpmTasks('grunt-contrib-less');

    // registrace tasku
    grunt.registerTask('default', ['less:devel']);

};